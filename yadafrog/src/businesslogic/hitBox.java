/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

/**
 *
 * @author Joeri Bouwman
 */
public class hitBox {
    protected int baseX;
    protected int baseY;
    protected int width;
    protected int height;
    
    public hitBox() {
    
    }
    
    public hitBox(int baseX, int baseY, int width, int height) {
        this.baseX = baseX;
        this.baseY = baseY;
        this.width = width; 
        this.height = height;
    }
    
    public int getBaseX() {
        return this.baseX;
    }
    public int getBaseY() {
        return this.baseY;
    }
    public int getWidth() {
        return this.width;
    }
    public int getHeight() {
        return this.height;
    }
    
    public void setBaseX(int baseX) {
        this.baseX = baseX;
    }
    public void setBaseY(int baseY) {
        this.baseY = baseY;
    }
    public void setWidth(int width) {
        this.width =  width;
    }
    public void setHeight(int height) {
        this.height = height;
    }

}

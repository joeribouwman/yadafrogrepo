/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

/**
 *
 * @author Joeri Bouwman
 */
public class hitTest {
    public Boolean testVertical(hitBox one, hitBox two) {
        if(
                //hier checken we of x tussen het object in zit daarna of de onderkant van object 1 tussen de min en max y zit van object 2
                (
                    one.getBaseX() >= two.getBaseX() && one.getBaseX() <= (two.getBaseX() + two.getWidth()) ||
                    (one.getBaseX() + one.getWidth()) <= (two.getBaseX() + two.getWidth()) && (one.getBaseX() + one.getWidth() >= two.getBaseX())
                ) && 
                    (one.getBaseY() + one.getHeight()) >= two.getBaseY() && (one.getBaseY() + one.getHeight()) < (two.getBaseY() + two.getHeight())
        ){
            return true;
        }
        return false;
    }
    
    public Boolean fullTest(hitBox one, hitBox two) {
        //vergelijkt de hoeken met elkaar dus zet altijd de kleinste als eerst anders pakt hij de hittest niet als het niet een hoek is
        if(
                //hier checken we of x tussen het object in zit daarna of de onderkant van object 1 tussen de min en max y zit van object 2
                (
                    one.getBaseX() >= two.getBaseX() && one.getBaseX() <= (two.getBaseX() + two.getWidth()) 
                    ||
                    (one.getBaseX() + one.getWidth()) <= (two.getBaseX() + two.getWidth()) && (one.getBaseX() + one.getWidth() >= two.getBaseX())
                ) && 
                (
                    (one.getBaseY() + one.getHeight()) >= two.getBaseY() && (one.getBaseY() + one.getHeight()) <= (two.getBaseY() + two.getHeight()) 
                    ||
                    one.getBaseY() >= two.getBaseY() && one.getBaseY() <= (two.getBaseY() + two.getHeight())
                )
        ){
            return true;
        }
        return false; 
    }
}
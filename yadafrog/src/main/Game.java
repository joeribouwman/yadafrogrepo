/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import presentation.Frog;
import presentation.GameLevel;

/**
 *
 * @author joeri
 */
public class Game {
    public GameLevel level;
    private Frog frog;
    
    public Game() {
        this.init();
        this.keyBoardInput();
        this.gameLoop();
    }
    
    private void init() {
       this.level = new GameLevel();
       this.frog = this.level.getFrog();
    }
    
    private void keyBoardInput() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(
            new KeyEventDispatcher() {
                @Override
                public boolean dispatchKeyEvent(KeyEvent ke) {
                    switch (ke.getID()) {
                        case KeyEvent.KEY_PRESSED:
                            if (ke.getKeyCode() == KeyEvent.VK_A) {
                                frog.setCurrentDirection("left");
                            }
                            if (ke.getKeyCode() == KeyEvent.VK_D) {
                                frog.setCurrentDirection("right");
                            }
                            if (ke.getKeyCode() == KeyEvent.VK_W) {
                                frog.jump();
                            }
                        break;
                        case KeyEvent.KEY_RELEASED:
                            if (ke.getKeyCode() == KeyEvent.VK_A) {
                                frog.setMoveLeft(false);
                            }
                            if (ke.getKeyCode() == KeyEvent.VK_D) {
                                frog.setMoveRight(false);
                            }
                        break;
                    }
                    return false;
                }
            }
        );
    }
    
    private void gameLoop() {
        while(true) {
            this.level.update();
            try {
                Thread.sleep(20); //the timing mechanism
            } catch (InterruptedException ex) {
                Logger.getLogger(Yadafrog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

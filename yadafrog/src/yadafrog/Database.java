//example hoe je deze shit kan gebruiken
/*
    Database db = new Database();
    List<Map<String, String>> result = dbObject.doQuery("SELECT * FROM user");
    for(Map<String, String> row : result) {
        for(String key : row.keySet()) {
            System.out.println(row.get(key));
        }
    }
*/
package yadafrog;

import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Database {
    Connection conn = null;
    
    public Database() {
        this.connect();
    }
    
    private Boolean connect() {
        try {
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost/javatestdb?user=root&password=");
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            return false;
        }
        return true;
    }

    public List getAsAssoc(String query) {
        List<Map<String, String>> resultList = new ArrayList<>();
        try {
            Statement st = this.conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();

        
            while(rs.next()) {
                Map<String, String> assoc = new HashMap<>();
                for(int i = 1; i <= columnCount; i++) {
                    assoc.put(rsmd.getColumnName(i), rs.getString(i));
                }
                resultList.add(assoc);
            }
        }
        catch(SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        
        return resultList;
    }
    
    public Boolean doQuery(String query, Map<String, String> Arguments) {
        try {
            Statement st = this.conn.createStatement();
            st.executeQuery(query);
            return true;
        }
        catch(SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return false;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yadafrog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Joeri Bouwman
 */
public class Window {
    public int windowHeight;
    public int windowWidth;
    
    public Window() {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        this.windowWidth = gd.getDisplayMode().getWidth() / 2;
        this.windowHeight = gd.getDisplayMode().getHeight() / 2;
        
        this.createWindow();
    }
    
    public void createWindow() {
        //Create and set up the window.
        JFrame frame = new JFrame("hoi");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel emptyLabel = new JLabel("");
        emptyLabel.setPreferredSize(new Dimension(this.windowWidth, this.windowHeight));
        
      
        frame.getContentPane().add(emptyLabel, BorderLayout.CENTER);
        
//        frame.add(new JLabel(new ImageIcon("yadafrog/resources/images/frog.png")));
        
        //Display the window.
        frame.pack();
        frame.setLocation(this.windowWidth / 2, this.windowHeight / 2);
        frame.setVisible(true);


    }
    
//    public void addToWindow() {
//         
//    }
}

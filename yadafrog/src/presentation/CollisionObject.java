/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import businesslogic.hitBox;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Joeri Bouwman
 */
public abstract class CollisionObject extends JLabel implements ICollisionObject {
    protected int x;
    protected int y;
    protected int width;
    protected int height;
    protected int speed;
    protected hitBox hitBox;
    protected int gravity;
    protected int baseGravity;
    
    protected void init() {
        this.x = 0;
        this.y = 0;
        this.speed = 8;
        this.gravity = 1;
        this.baseGravity = -15;
    }
    
    protected void loadDefaultImage(String imagePath) {
        try {
            BufferedImage myPicture = ImageIO.read(new File(imagePath));
            this.setIcon(new ImageIcon(myPicture));
        }
        catch (IOException ex) {
            Logger.getLogger(Level.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void resetPosition() {
        this.x = 0;
        this.y = 0;
    }
    
    public void randomX() {
        int defaultRange = GameLevel.LEVELWIDTH - (this.width / 2);
        this.randomX(defaultRange);
    }
    
    public void randomX(int range) {
       int random = (int)(Math.random() * range);
       this.x = random;
    }
    
    public void randomY() {
        int defaultRange = GameLevel.LEVELHEIGHT - (this.height / 2);
        this.randomX(defaultRange);
    }
    
    public void randomY(int range) {
        int random = (int)(Math.random() * range);
        this.y = random;
    }
    
    public void randomYtopHalf() {
        int defaultRange = (GameLevel.LEVELHEIGHT - (this.height / 2)) / 2;
        this.randomY(defaultRange);
    }
    
    public void setX(int x) {
        this.x = x;
    }
    @Override
    public int getX() {
        return this.x;
    }
    
    public void setY(int y) {
        this.y = y;
    }
    @Override
    public int getY() {
        return this.y;
    }

    public hitBox getHitBox() {
        return this.hitBox;
    }
    
    public int getGravity() {
        return this.gravity;
    }
    
    public void setGravity(int gravity) {
        this.gravity = gravity;
    }
    
    public int getSpeed() {
        return this.speed;
    }
    
    public void setSpeed(int speed) {
        this.speed = speed;
    }
}

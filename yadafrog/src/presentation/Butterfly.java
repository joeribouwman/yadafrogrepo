/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import businesslogic.hitBox;

/**
 *
 * @author Joeri Bouwman
 */
public class Butterfly extends CollisionObject {
    public Butterfly() {
        this.loadDefaultImage("src/resources/images/butterfly.png");
        this.init();
    }
    
    @Override
    protected void init() {
        super.init();
        this.height = 27;
        this.width = 25;
        this.hitBox = new hitBox(this.x, this.y, this.width, this.height);
        this.setBounds(this.x, this.y, this.width, this.height);
    }
    
    @Override
    public void update() {
//        this.y += this.gravity;
    
        this.hitBox.setBaseX(this.x);
        this.hitBox.setBaseY(this.y);
        this.setBounds(this.x, this.y, this.width, this.height);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;


import businesslogic.hitBox;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author joeri
 */
public class Frog extends CollisionObject {
    private Boolean moveLeft = false;
    private Boolean moveRight = false;
    private ImageIconCollection imgCol;
    private Map<String, String> imgUrls; 
    private Boolean isGravity;
    private String currentDirection;
    private int jumpSpeedLimit = 15;
    
    public Frog() {
        this.imgCol = new ImageIconCollection();
        this.imgUrls = new HashMap<>();
        this.imgUrls.put("left", "src/resources/images/smallfrogleft.png");
        this.imgUrls.put("right", "src/resources/images/smallfrogright.png");
        this.imgCol.addCollection(imgUrls);

        this.init();
    }
    
    @Override
    protected void init() {
        super.init();
        this.isGravity = true;
        this.height = 50;
        this.width = 88;
        this.hitBox = new hitBox(this.x, this.y, this.width, this.height);
        this.setBounds(this.x, this.y, this.width, this.height);
        this.setIcon(this.imgCol.getIcon("left"));
    }
    
    public void update() {
        if(this.moveLeft) {
            this.x -= speed;
            if(this.x < -100)
                this.x = 550;
        }
        if(this.moveRight) {
            this.x += speed;
            if(this.x > 550)
                this.x = -100;
        }
        if(this.isGravity) {
            if(this.gravity < this.jumpSpeedLimit)
                this.gravity += 1;
            this.y += gravity;
        }
        this.hitBox.setBaseX(this.x);
        this.hitBox.setBaseY(this.y);
        this.setBounds(this.x, this.y, this.width, this.height);
    }
    
    public void setMoveLeft(Boolean moveLeft) {
        this.moveLeft = moveLeft;
        this.setIcon(this.imgCol.getIcon("left"));
    }
    
    public void setMoveRight(Boolean moveRight) {
        this.moveRight = moveRight;
        this.setIcon(this.imgCol.getIcon("right"));
    }

    public void jump() {
        if(!isGravity) {
            this.gravity = this.baseGravity;
            this.isGravity = true;
        }
    }
    
    public void resetJump() {
        this.gravity = 0;
        this.isGravity = false;
    }
    
    public void setIsGravity(Boolean isGravity) {
        this.isGravity = isGravity;
    }
    
    public Boolean getIsGravity() {
        return this.isGravity;
    }
    
    public String getCurrentDirection() {
        return currentDirection;
    }

    public void setCurrentDirection(String direction) {
        this.currentDirection = direction;
        if("left".equals(direction))
            this.setMoveLeft(true);
        else if("right".equals(direction))
            this.setMoveRight(true);
    }
}

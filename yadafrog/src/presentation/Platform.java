/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import businesslogic.hitBox;

/**
 *
 * @author Joeri Bouwman
 */
public class Platform extends CollisionObject {
    
    public Platform() {
        this.loadImage();
        this.init();
    }
    
    protected void loadImage() {
        this.loadDefaultImage("src/resources/images/platform.png");
    }
    
    @Override
    protected void init() {
        super.init();
        this.height = 30;
        this.width = 160;
        this.hitBox = new hitBox(this.x, this.y, this.width, this.height);
        this.setBounds(this.x, this.y, this.width, this.height);
    }
    
    @Override
    public void update() {
        this.y += this.gravity;
        this.hitBox.setBaseX(this.x);
        this.hitBox.setBaseY(this.y);
        this.setBounds(this.x, this.y, this.width, this.height);
    }
}

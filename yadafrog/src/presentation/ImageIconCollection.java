/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class ImageIconCollection {
    private Map<String, ImageIcon> imagesMap; 
    
    public ImageIconCollection() {
        this.imagesMap = new HashMap<>();
    }
    
    public void addCollection(Map<String, String> imgmap) {
          try {
            for (Map.Entry<String, String> entry : imgmap.entrySet()) {
                BufferedImage myPicture = ImageIO.read(new File(entry.getValue()));
                this.imagesMap.put(entry.getKey(), new ImageIcon(myPicture));
            }
        }
        catch (IOException ex) {
            Logger.getLogger(Level.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ImageIcon getIcon(String key) {
        return this.imagesMap.get(key);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

/**
 *
 * @author Joeri Bouwman
 */
public class MovingPlatform extends Platform {
    private int movingRange;
    
    public static enum SideDirections {LEFT, RIGHT, NONE};
    public static enum UpDirections {UP, DOWN, NONE};
    private SideDirections sideDirection;
    private UpDirections upDirection;
    
    private int startPointX, startPointY, endPointX, endPointY;
 
    public MovingPlatform(SideDirections sideDirection, UpDirections upDirection) {
        super();
        this.sideDirection = sideDirection;
        this.upDirection = upDirection;
    }
    
    @Override
    protected void init() {
        super.init();
        this.movingRange = 400;
        this.startPointX = this.x;
        this.startPointY = this.y;
        this.endPointX = this.x + movingRange;
        this.endPointY = this.y + movingRange;
        this.speed = 2;
    }
    
    @Override
    public void update() {
        switch(this.sideDirection) {
            case LEFT:
                if(this.x < this.startPointX)
                    this.sideDirection = SideDirections.RIGHT;
                this.x -= this.speed;
            break;
            case RIGHT:
                if(this.x > this.endPointX)
                    this.sideDirection = SideDirections.LEFT;
                this.x += this.speed;
            break;
            case NONE: 
                
            break;
        }
        
        switch(this.upDirection) {
            case UP:
                if(this.y < this.startPointY)
                    this.upDirection = UpDirections.DOWN;
                this.y -= this.speed;
            break;
            case DOWN:
                if(this.y > this.endPointY)
                    this.upDirection = UpDirections.UP;
                this.y += this.speed;
            break;
        }
        
        super.update();
    }
    
    public SideDirections getSideDirection() {
        return this.sideDirection;
    }
    
    public UpDirections getUpDirection() {
        return this.upDirection;
    }
}
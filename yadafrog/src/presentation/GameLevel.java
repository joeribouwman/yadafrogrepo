package presentation;

import businesslogic.hitTest;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.*;

public class GameLevel {
    private JFrame frame;
    private JPanel panel;
    private JButton b1;
    private JLabel lab;
    private Frog frog;
    private List<CollisionObject> collisionObjectArr;

    private final hitTest hitTest;
    
    public static final int LEVELHEIGHT  = 800;
    public static final int LEVELWIDTH = 600;
    
    private int platformcounter;   
    private int score;
  
    
    public GameLevel() {
        this.hitTest = new hitTest();
        this.init();
    }
    
    private void init() {
        this.panel = new JPanel();
       
        
        this.collisionObjectArr = new ArrayList();
        
        int amountOfPlatforms = GameLevel.LEVELHEIGHT / 100; 
        for(int i = 0; i < amountOfPlatforms; i++) {
            int yValue;
            int xValue;
            if(i == amountOfPlatforms - 1) {
                xValue = 0;
                yValue = GameLevel.LEVELHEIGHT - 100;
            }
            else {
                yValue = i*100;
                xValue = (int)(Math.random() * 550 + 1);
            }
            Platform platform = new Platform();
            platform.setY(yValue);
            platform.setX(xValue);
            collisionObjectArr.add(platform);
            p.add(platform);
        }
        
        this.frog = new Frog();
     
        this.frame = new JFrame("yadayada");
        this.frame.setSize(600,800);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
      
        this.panel.setBackground(Color.YELLOW);
    
//        b1 = new JButton("test");
//        b1.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("hoi");
//            }
//        });
        
//        lab = new JLabel("This is test label");
        this.panel.add(this.frog);
       
      
//        p.add(b1);
//        p.add(lab);
        this.frame.add(panel);
        this.frame.setVisible(true);
    }
    
    public void update() {
        Boolean hasCollision = false;
        
        this.frog.update();

        for (CollisionObject collisionObject : new ArrayList<CollisionObject>(this.collisionObjectArr)) {
            collisionObject.update();
            
            if(collisionObject instanceof Butterfly) {
                if(this.hitTest.fullTest(collisionObject.getHitBox(), this.frog.getHitBox())) {
                    this.score++;
                    this.removeObject(collisionObject);
                }
            }
            else if(collisionObject instanceof SpikeBall) {
                if(this.hitTest.fullTest(collisionObject.getHitBox(), this.frog.getHitBox())) {
                    this.gameOverEvent();
                }
            }
            else if(collisionObject instanceof Platform) { 
                if(this.hitTest.testVertical(this.frog.getHitBox(), collisionObject.getHitBox()) && this.frog.getGravity() > -1) {
                    hasCollision = true;
                    this.frog.resetJump();
                    this.frog.setY(collisionObject.getY() - this.frog.getHeight() + 2);
                    if(collisionObject instanceof MovingPlatform) {
                        MovingPlatform platform2 = (MovingPlatform)collisionObject;     
                        if(platform2.getSideDirection() != MovingPlatform.SideDirections.NONE) {
                            int sideMovement = platform2.getSideDirection() == MovingPlatform.SideDirections.RIGHT ? platform2.getSpeed() : - (platform2.getSpeed());
                            this.frog.setX(this.frog.getX() + sideMovement);
                        }
                        if(platform2.getUpDirection() != MovingPlatform.UpDirections.NONE) {
                            int upMovement = platform2.getUpDirection() == MovingPlatform.UpDirections.DOWN ? platform2.getSpeed() : - (platform2.getSpeed());
                            this.frog.setY(this.frog.getY() + upMovement);
                        }   
                    }
                }
                if(collisionObject.getY() > GameLevel.LEVELHEIGHT) {
                    this.platformCycle((Platform)collisionObject);
                }
            }
        
        }
        
        if(!this.frog.getIsGravity() && hasCollision == false)
            this.frog.setIsGravity(true);
    }
    
    public Frog getFrog() {
        return this.frog;
    }
    
    private void platformCycle(Platform platform) {
        this.platformcounter++;
           
        //one outa ten should be moving
        int movingPlatformOdds = (int)(Math.random() * 10);
        System.out.println(movingPlatformOdds);
        if(movingPlatformOdds == 1 && !(platform instanceof MovingPlatform)) {
            this.removeObject(platform);
            platform = (MovingPlatform) this.addObject(new MovingPlatform(MovingPlatform.SideDirections.LEFT, MovingPlatform.UpDirections.NONE));
        }
        else {
            if(platform instanceof MovingPlatform) {
                this.removeObject(platform);
                platform = (Platform) this.addObject(new Platform());
            }
            platform.resetPosition();
            platform.randomX();
        }
        
        if(this.platformcounter % 2 == 0) {
            SpikeBall spike = new SpikeBall();
            p.add(spike);
            spike.randomX();
            spike.randomYtopHalf();
            this.addObject(spike);
        }
        
        if(this.platformcounter % 5 == 0) {
            Butterfly butterfly = new Butterfly();
            butterfly.randomX();
            butterfly.randomYtopHalf();
            this.addObject(butterfly);
        }
    }
    
    private CollisionObject addObject(CollisionObject obj) {
        this.collisionObjectArr.add(obj);
        p.add(obj);
        return obj;
    }
    
    private void removeObject(CollisionObject obj) {
        this.collisionObjectArr.remove(obj);
        p.remove(obj);
    }
    
    private void gameOverEvent() {
        System.out.println("u diez");
    }
}